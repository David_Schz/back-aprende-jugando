from .db import db
from flask_bcrypt import generate_password_hash, check_password_hash

class Rol(db.Document):
    name = db.StringField(required=True, unique=True)
    description = db.StringField(required=True)


class User(db.Document):
    nickname = db.StringField(required=True, unique=True)
    email = db.StringField(required=True, unique=True)
    password = db.StringField(required=True)
    rol = db.ReferenceField(Rol)
    is_federated_account = db.BooleanField(required=True)

    def hash_password(self):
        self.password = generate_password_hash(self.password).decode('utf8')
 
    def check_password(self, password):
        return check_password_hash(self.password, password)